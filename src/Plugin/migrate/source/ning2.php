<?php
/**
 * @file
 * Contains \Drupal\migrate_ning2\Plugin\migrate\source\ning2.
 */

namespace Drupal\migrate_ning2\Plugin\migrate\source;


use Drupal\migrate\Plugin\Migration;
use Drupal\migrate\Row;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;

/**
 * Source for ning2.
 *
 * If the CSV file contains non-ASCII characters, make sure it includes a
 * UTF BOM (Byte Order Marker) so they are interpreted correctly.
 *
 * @MigrateSource(
 *   id = "ning2"
 * )
 */
class ning2 extends CSV {

    public $config_csvdir;
    public $config_imgdir;

    /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Migration $migration) {

    // drush_print_r( $configuration );

    // @@@@: this should be taken from the migration group configuration (did not work at the time ..)
    $this->config_csvdir = '/home/vagrant/sites/GPLDATA/out/ningcsv';
    $this->config_imgdir = '/home/vagrant/sites/GPLDATA/out/ningimg';

    if (! empty($this->config_csvdir)) {
      $path = $configuration['path'];
      $configuration['path'] = $this->config_csvdir . '/' . $path;
    }
    $configuration['delimiter'] = "\t";

    // CSV (already) uses (full) path
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }


  /**
   * {@inheritdoc}
   */
  public function getIDs() {
    $ids = [];
    foreach ($this->configuration['keys'] as $key) {
      $key = ltrim($key); // utf8 BOM header no correctly handled yet 
      $ids[$key]['type'] = 'string';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function checkRequirements() // we use the migration groups only for sorting
  {}

  /**
   * flag for detecting first row for special actions
   */
  protected $firstRow = true;

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    //
    // If it’s a comment (cid_ exists), set it as db start id
    // (it’s a hack, but we need this, because we have lot’s of links to comments too,
    // and we put a tremendous effort in the preprocessor to translate them.
    // unfortunately Drupal 8 (like 7) does not “legally” allow to write the cid).
    if( $this->firstRow )
    {
      $this->firstRow = false;
      if( $row->hasSourceProperty('_cid') ) {
        $cid = $row->getSourceProperty('_cid');
        if( !empty($cid) ) { 
          db_query('ALTER TABLE comment AUTO_INCREMENT = ' . $cid . ';')->execute();
        }
      }
    }

    // apply common path transformation to all image_ fields
    // we don't want to repeat this in all migrations.
    // the config_* should go to migration group, though.
    if( $row->hasSourceProperty('_image') ) {
      $image = $row->getSourceProperty('_image');
      if( !empty($image) ) {
        $srcpath = $this->config_imgdir . '/' .  $image;
        $dstpath = 'public://' . $image;
        $row->setSourceProperty('_srcpath', $srcpath);
        $row->setSourceProperty('_dstpath', $dstpath);
      }
    }

    $props = $row->getSource();
    
    // test (explode in process plugin does not work for some reason)
    // if ($value = $row->getSourceProperty('forum_ids_')) {
    //  $row->setSourceProperty('forum_ids_', explode('#', $value));
    //}

    // drush_print_r($row);
  
    return parent::prepareRow($row);    
  }
}

