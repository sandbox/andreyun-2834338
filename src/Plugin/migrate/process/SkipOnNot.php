<?php

/**
 * @file
 * Contains \Drupal\migrate_ning2\Plugin\migrate\process\SkipOnNot.
 */

namespace Drupal\migrate_ning2\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * If the source evaluates to empty, we skip processing or the whole row.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_on_not"
 * )
 */
class SkipOnNot extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $compare = $this->configuration['compare'];
    if ($value != $compare) {
      throw new MigrateSkipRowException();
    }
    $value = $row->getSourceProperty($this->configuration['value']);
    return $value;
  }

}
