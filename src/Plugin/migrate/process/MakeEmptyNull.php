<?php

/**
 * @file
 * Contains \Drupal\migrate_ning2\Plugin\migrate\process\MakeEmptyNull.
 */

namespace Drupal\migrate_ning2\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * If the source evaluates to empty, we skip processing or the whole row.
 *
 * @MigrateProcessPlugin(
 *   id = "make_empty_null"
 * )
 */
class MakeEmptyNull extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!$value) {
      $value = NULL;
    }
    return $value;
  }

}
