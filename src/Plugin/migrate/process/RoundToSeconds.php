<?php

/**
 * @file
 * Contains \Drupal\migrate_ning2\Plugin\migrate\process\RoundToSeconds.
 */

namespace Drupal\migrate_ning2\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * If the source evaluates to empty, we skip processing or the whole row.
 *
 * @MigrateProcessPlugin(
 *   id = "round_to_seconds"
 * )
 */
class RoundToSeconds extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // $value: cut off .<nnn>Z and replace by Z
    if( is_null($value) )
    {
      return $value;
    }

    // $old = $value;

    $pos = strpos($value,'.');
    if( FALSE !== $pos )
    {
      $value = substr($value,0,$pos); // . 'Z';
    }

    // drush_print_r( $old . ' -> ' . $value );
    
    return $value;
  }

}
